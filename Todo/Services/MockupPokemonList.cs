﻿using System;
using System.Collections.Generic;
using Todo.Models;
using System.Linq;

namespace Todo.Services
{
    public class MockupPokemonList : IPokemonList
    {
        private List<PokemonModel> _pokemonList;

        public MockupPokemonList()
        {
            _pokemonList = new List<PokemonModel>
            {
                new PokemonModel() { Id = 1, Name = "Pikachu", Hp = 200, Damage = 20, Type = PokemonType.Electrik, PhotoPath = "pikachu.png" },
                new PokemonModel() { Id = 2, Name = "Salamèche", Hp = 200, Damage = 20, Type = PokemonType.Feu, PhotoPath = "salameche.png" },
                new PokemonModel() { Id = 3, Name = "Kicklee", Hp = 150, Damage = 35, Type = PokemonType.Combat, PhotoPath = "kicklee.png" },
                new PokemonModel() { Id = 4, Name = "Tygnon", Hp = 150, Damage = 35, Type = PokemonType.Combat, PhotoPath = "tygnon.png" },
                new PokemonModel() { Id = 5, Name = "Carapuce", Hp = 200, Damage = 20, Type = PokemonType.Eau, PhotoPath = "carapuce.png" },
                new PokemonModel() { Id = 6, Name = "Abra", Hp = 150, Damage = 10, Type = PokemonType.Psy, PhotoPath = "abra.png" },
                new PokemonModel() { Id = 7, Name = "Abo", Hp = 150, Damage = 20, Type = PokemonType.Poison, PhotoPath = "abo.png" },
                new PokemonModel() { Id = 8, Name = "Ronflex", Hp = 300, Damage = 20, Type = PokemonType.Normal, PhotoPath = "ronflex.png" },
                new PokemonModel() { Id = 9, Name = "Bulbizarre", Hp = 150, Damage = 20, Type = PokemonType.Plante, PhotoPath = "bulbizarre.png" },
                new PokemonModel() { Id = 10, Name = "Roucoule", Hp = 150, Damage = 20, Type = PokemonType.Vol, PhotoPath = "roucoule.png" },
                new PokemonModel() { Id = 11, Name = "Evoli", Hp = 150, Damage = 20, Type = PokemonType.Normal, PhotoPath = "evoli.png" },
                new PokemonModel() { Id = 12, Name = "Cizayox", Hp = 400, Damage = 60, Type = PokemonType.Insecte, PhotoPath = "cizayox.png" }
            };
        }

        public IEnumerable<PokemonModel> GetAllPokemons()
        {
            return _pokemonList;
        }

        public PokemonModel GetPokemon(int id)
        {
            return _pokemonList.FirstOrDefault(p => p.Id == id);
        }

        public PokemonModel UpdatePokemon(PokemonModel updatedPokemon)
        {
            PokemonModel pokemon = _pokemonList.FirstOrDefault(p => p.Id == updatedPokemon.Id);
            if (pokemon != null)
            {
                pokemon.Name = updatedPokemon.Name;
                pokemon.Damage = updatedPokemon.Damage;
                pokemon.Hp = updatedPokemon.Hp;
                pokemon.Type = updatedPokemon.Type;
            }

            return pokemon;
        }

        public void DeletePokemon(int id)
        {
            PokemonModel pokemon = _pokemonList.FirstOrDefault(p => p.Id == id);
            if (pokemon != null)
            {
                _pokemonList.Remove(pokemon);
            }
        }

        public void AddPokemon(PokemonModel pokemon, int id)
        {
            pokemon.Id = id;
            _pokemonList.Add(pokemon);
            IPokemonList.Id++;
        }

       

    }
}
