﻿using System;
using System.Collections.Generic;

namespace Todo.Services
{
    public interface IPokemonList
    {
        static int Id { get; set; } = 13;
        IEnumerable<PokemonModel> GetAllPokemons();
        PokemonModel GetPokemon(int id);
        PokemonModel UpdatePokemon(PokemonModel pokemon);
        void AddPokemon(PokemonModel pokemon, int id);
        void DeletePokemon(int id);
    }
}
