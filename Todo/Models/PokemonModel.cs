﻿using System;
using Todo.Models;

namespace Todo
{
    public class PokemonModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Hp { get; set; }
        public PokemonType Type { get; set; }
        public int Damage { get; set; }
        public string PhotoPath { get; set; } 
    }
}
