﻿using System;
namespace Todo.Models
{
    public enum PokemonType
    {
        Feu,
        Eau,
        Plante,
        Insecte,
        Normal,
        Electrik,
        Poison,
        Vol,
        Combat,
        Psy
    }
}
