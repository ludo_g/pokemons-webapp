﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Todo.Services;
using Todo.Models;

namespace Todo.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IPokemonList pokemonList;
        public IEnumerable<PokemonModel> Pokemons { get; set; }
        public int PokemonId { get; set; }
        public IndexModel(IPokemonList pokemonList)
        {
            this.pokemonList = pokemonList;
        }

        public void OnGet()
        {
            Pokemons = pokemonList.GetAllPokemons();
        }

        public string TypeColor(PokemonType type)
        {
            switch (type)
            {
                case PokemonType.Eau:
                    return "blue";
                case PokemonType.Feu:
                    return "#DD3C3C";
                case PokemonType.Plante:
                    return "green";
                case PokemonType.Insecte:
                    return "#2F4F4F";
                case PokemonType.Vol:
                    return "#E0FDFE";
                case PokemonType.Psy:
                    return "fuchsia";
                case PokemonType.Electrik:
                    return "#DAA52C";
                case PokemonType.Combat:
                    return "#8B221A";
                case PokemonType.Poison:
                    return "#8D338B";
                case PokemonType.Normal:
                    return "#A9A9A9";
                default:
                    return "";
            }
        }


    }
}
