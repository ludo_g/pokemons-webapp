﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Todo.Services;

namespace Todo.Pages.Pokemon
{
    public class EditPokemonModel : PageModel
    {
        private readonly IPokemonList pokemonList;
        public PokemonModel Pokemon { get; set; }

        [BindProperty]
        public IFormFile Photo { get; set; }

        [BindProperty(SupportsGet = true)]
        public int Id { get; set; }

        [BindProperty]
        public PokemonModel updatedPokemon { get; set; }

        public EditPokemonModel(IPokemonList pokemonList)
        {
            this.pokemonList = pokemonList;
        }

        public IActionResult OnGet()
        {
            Pokemon = pokemonList.GetPokemon(Id);
            if(Pokemon == null)
            {
                return RedirectToPage("/NotFound");
            }

            return Page();
        }

        public IActionResult OnPost()
        {
            //if(Photo != null)
            //{
            //    updatedPokemon.PhotoPath = "";
            //} else
            //{
            //    updatedPokemon.PhotoPath = Pokemon.PhotoPath;
            //}
            pokemonList.UpdatePokemon(updatedPokemon);
            return RedirectToPage("/Index");
        }
    }
}
