﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Todo.Services;

namespace Todo.Pages.Pokemon
{
    public class DeletePokemonModel : PageModel
    {
        private readonly IPokemonList pokemonList;

        [BindProperty(SupportsGet=true)]
        public int Id { get; set; }


        public DeletePokemonModel(IPokemonList pokemonList)
        {
            this.pokemonList = pokemonList;
        }

        public IActionResult OnGet()
        {
            pokemonList.DeletePokemon(Id);
            return RedirectToPage("/Index");
        }
    }
}
