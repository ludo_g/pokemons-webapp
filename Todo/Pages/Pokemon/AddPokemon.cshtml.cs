﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Todo.Services;

namespace Todo.Pages.Pokemon
{
    public class AddPokemonModel : PageModel
    {
        private readonly IPokemonList pokemonList;
        [BindProperty]
        public PokemonModel Pokemon { get; set; }

        public AddPokemonModel(IPokemonList pokemonList)
        {
            this.pokemonList = pokemonList;
        }

        public void OnGet()
        {
        }

        public IActionResult OnPost()
        {

            pokemonList.AddPokemon(Pokemon, IPokemonList.Id);
            return RedirectToPage("/Pokemon/ShowPokemon", new { Id = Pokemon.Id });
        }
    }
}
