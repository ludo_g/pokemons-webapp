﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Todo.Services;
using Todo.Models;

namespace Todo.Pages.Pokemon
{
    public class ShowPokemonModel : PageModel
    {
        private readonly IPokemonList pokemonList;

        [BindProperty(SupportsGet=true)]
        public int Id { get; set; }
        public PokemonModel Pokemon { get; private set; }

        public ShowPokemonModel(IPokemonList pokemonList)
        {
            this.pokemonList = pokemonList;
        }

        public IActionResult OnGet()
        {
            Pokemon = pokemonList.GetPokemon(Id);

            if (Pokemon == null)
            {
                return RedirectToPage("/NotFound");
            }

            return Page();
        }
    }
}
